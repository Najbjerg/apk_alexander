#include "DrugTest.hpp"

namespace dl = DrugLord;

int main(int arcv, char** args)
{
    testing::InitGoogleTest(&arcv, args);
    return RUN_ALL_TESTS();
}