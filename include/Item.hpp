#ifndef __ITEM_HPP__
#define __ITEM_HPP__

#include <iostream>

//**************************************************//
//                     Item.hpp                     //
//  Base item class. Container keeps pointers to    //
//  this class. From this class all items are       //
//  derived.                                        //
//  This is also the place where the user-defined   //
//  literals are implemented.                       //
//**************************************************//

/**
 * @brief Base item class. Container keeps pointers to this class. From this class all items are derived.
 * 
 */
class Item
{
public:
    /**
     * @brief Construct a new Item object
     */
    Item()
    {
        setSellPrice(0);
        setBasePrice(0);
        setQuantity(0);
    }

    /**
     * @brief Returning the price the shop will give for the Drug.
     * 
     * @return double is the returned sell price.
     */
    virtual double getSellPrice() const { return sellPrice; }

    /**
     * @brief Setting the price the shop will give for the Drug.
     * 
     * @param newPrice is the new price.
     */
    virtual void setSellPrice(double newPrice) { sellPrice = newPrice; }

    /**
     * @brief Returning the price the shop wants for the Drug.
     * 
     * @return double is the returned base price.
     */
    virtual double getBasePrice() const { return basePrice; }

    /**
     * @brief Setting the price the shop wants for the Drug.
     * 
     * @param newPrice 
     */
    virtual void setBasePrice(double newPrice) { basePrice = newPrice; }

    /**
     * @brief Returning the amount of the drug kept by either the store or the player.
     * 
     * @return int is the amount of the drug kept.
     */
    virtual int getQuantity() const { return quantity; }                          

    /**
     * @brief Setting the amount of the drug kept by either the store or the player.
     * 
     * @param newQuantity the new amount of the drug.
     */
    virtual void setQuantity(long double newQuantity){ quantity = newQuantity;}

    /**
     * @brief Prints out that this is an Item. More useful when overridden in Drug.
     * 
     */
    virtual void printType()
    {
        std::cout << "This is object of class Item." << std::endl;
    }

    /**
     * @brief Set the Base Price and the Sell Price for this object to a hardcoded value (10.0).
     * 
     */
    virtual void setStartPrice()
    {
        setSellPrice(10.0);
        setBasePrice(10.0);
    }

    // /**
    //  * @brief Returns Item pointer for this object.
    //  * 
    //  * @return const Item* is the pointer for this object.
    //  */
    // virtual Item* clone() { return this;}

    /**
     * @brief This method is for the container to be able to create a type Drug<weed, lsd, mushroom, heroine, cocaine>
     *        from the Item*. Wanted to use covariance to return pointer to object, but Item* cannot be used to 
     *        return a Drug*.
     * 
     * @param weed variable is set to the value of the weedVal in the Item object.
     * @param lsd variable is set to the value of the lsdVal in the Item object.
     * @param mushroom variable is set to the value of the mushroomVal in the Item object.
     * @param heroine variable is set to the value of the heroineVal in the Item object.
     * @param cocaine variable is set to the value of the cocaineVal in the Item object.
     */
    virtual void checkValues(int& weed, int& lsd, int& mushroom, int& heroine, int& cocaine)
    {
        weed = 0;
        lsd = 0;
        mushroom = 0;
        heroine = 0;
        cocaine = 0;
    }

    double sellPrice = 10.0;             // Price if sold by player
    double basePrice = 10.0;             // Price if sold by shop
    double quantity = 0;                 // Grams of the item kept by either the store or the player.

    /**
     * @brief Adding two of same type. Quantities summed in this object.
     * 
     * @param refItem is the right hand side of the operator.
     * @return Item& is the left hand side of the operator. This is where the quantity goes.
     */
    virtual Item & operator += (const Item & refItem)
    {
        int tempQuantity = refItem.getQuantity();
        tempQuantity += getQuantity();
        setQuantity(tempQuantity);
        return *this;
    }

    /**
     * @brief Subtracting two of same type. Quantities subtracted from this object.
     * 
     * @param refItem is the right hand side of the operator.
     * @return Item& is the left hand side of the operator. This is where the quantity is subtracted from.
     */
    virtual Item & operator -= (const Item & refItem)
    {
        int tempQuantity = getQuantity();
        tempQuantity -= refItem.getQuantity();
        setQuantity(tempQuantity);
        return *this;
    }

    virtual void updatePrice(){};
};


//*******************************************************************************
// U S E R   D E F I N E D   L I T E R A L S
//*******************************************************************************

/**
 * @brief Returns x g as grams.
 */
constexpr long double operator"" _g(long double x)          // constexpr == calculations at compile time if possible.
{
    return x;
}

/**
 * @brief Returns x kg as grams.
 */
constexpr long double operator"" _kg(long double x)         // constexpr == calculations at compile time if possible.
{
    return x * 1000.0_g;
}

/**
 * @brief Returns x tonnes as grams.
 */
constexpr long double operator"" _tonne(long double x)      // constexpr == calculations at compile time if possible.
{
    return x * 1000.0_kg;
}

/**
 * @brief Returns x mg as grams.
 */
constexpr long double operator"" _mg(long double x)         // constexpr == calculations at compile time if possible.
{
    return x / 1000;
}

/**
 * @brief Returns x kr as kr.
 * 
 */
constexpr long double operator"" _kr(long double x)
{
    return x;
}

/**
 * @brief Returns x usd as kr.
 * 
 */
constexpr long double operator"" _usd(long double x)
{
    return x * 6.68_kr;
}

/**
 * @brief Enables printing of entire Item.
 * 
 * @param os is the output stream.
 * @param item is the item to be printed out to the os.
 * @return std::ostream& is the reference to enable concatination of the ostream.
 */
std::ostream& operator<<(std::ostream& os, const Item& item)
{
    os << "Quantity: " << item.getQuantity() << " grams, Price: " << item.getSellPrice() << " per gram.";
    return os;
}
#endif // __ITEM_HPP__