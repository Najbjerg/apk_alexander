#ifndef __TYPELIST_HPP
#define __TYPELIST_HPP

//*******************************************************************************
// T Y P E L I S T
//*******************************************************************************

    /**
     * @brief TypeList for keeping track of the allowed types of drugs.
     * 
     * @tparam L is the first value of the TypeList. It's a type.
     * @tparam R is the rest of the TypeList - either a TypeList or a NullType.
     */
    template <typename L, typename R>
    struct TypeList 
    {
        typedef L First;
        typedef R Rest;
    };

    // Definition of the nulltype, that acts as a stopping class from recursion.
    struct NullType{};

    /**
     * @brief Runs through TypeList to check if it contains the TypeInQuestion.
     * 
     * @tparam Typelist is the TypeList to run through.
     * @tparam TypeInQuestion is the type checked for in the TypeList.
     */
    template< typename Typelist, typename TypeInQuestion >
    struct Contains
    {
        static const bool value = ( std::is_same<typename Typelist::First, TypeInQuestion>::value ? true : Contains<typename Typelist::Rest, TypeInQuestion>::value);
    };

    template< typename TypeInQuestion >
    struct Contains<NullType, TypeInQuestion>
    {
        static const bool value = false;
    };

    // Macros for different sizes of TypeLists (For easier reading)
    #define TYPELIST1(arg1) TypeList< arg1, NullType >     // Smallest TypeList: Only one argument and NullType
    #define TYPELIST2(arg1, arg2) TypeList< arg1, TYPELIST1(arg2) >
    #define TYPELIST3(arg1, arg2, arg3) TypeList< arg1, TYPELIST2(arg2, arg3) >

    #endif //__TYPELIST_HPP__