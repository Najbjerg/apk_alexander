#ifndef __DRUG_HPP__
#define __DRUG_HPP__

#include "Item.hpp"
#include "typelist.hpp"
#include <stdexcept>

namespace DrugLord
{
    //**************************************************//
    //                     Drug.hpp                     //
    //  Drug class derived from Item class. Keeps info  //
    //  on the quantity of the drug. All types of drugs //
    //  are derived from this class.                    //
    //  All specializations of the Drug class is also   //
    //  implemented here.                               //
    //**************************************************//


    /**
     * @brief Drug class defines what drug and how much of it.
     * 
     * Drug class is derived from Item class. Keeps info on the quantity of the drug. 
     * All types of drugs are derived from this class. 
     * All specializations of the Drug class is also implemented here.
     * 
     * @tparam Weed is set if Drug is Weed or a subtype of Weed.
     * @tparam LSD is set if Drug is LSD or a subtype of LSD.
     * @tparam Mushroom is set if Drug is Mushroom or a subtype of Mushroom.
     * @tparam Heroine is set if Drug is Heroine or a subtype of Heroine.
     * @tparam Cocaine is set if Drug is Cocaine or a subtype of Cocaine.
     */
    template<int Weed, int LSD, int Mushroom, int Heroine, int Cocaine>
    class Drug : public Item
    {
    public:
        Drug()
        {
            // All attributes from Item class is set to 0 by default.
            setStartPrice();
        }

        /**
         * @brief Construct a new Drug object.
         * 
         * @param quantity Set amount of drug in grams.
         */
        Drug(int quantity)
        { 
            setQuantity(quantity);
            setStartPrice();
        };

        /**
         * @brief Prints out the five type-defining variables (Weed, LSD, Mushroom, Heroine and Cocaine) to the console.
         */
        void printType() override
        {
            if(weedVal == 1 && lsdVal == 0 && mushroomVal == 0 && heroineVal == 0 && cocaineVal == 0) // Weed
            {
                std::cout << "Weed:            ";
            }
            else if(weedVal == 0 && lsdVal == 1 && mushroomVal == 0 && heroineVal == 0 && cocaineVal == 0) // LSD
            {
                std::cout << "LSD:             ";
            }
            else if(weedVal == 0 && lsdVal == 0 && mushroomVal == 1 && heroineVal == 0 && cocaineVal == 0) // Mushroom
            {
                std::cout << "Mushroom:        ";
            }
            else if(weedVal == 0 && lsdVal == 0 && mushroomVal == 0 && heroineVal == 1 && cocaineVal == 0) // Heroine
            {
                std::cout << "Heroine:         ";
            }
            else if(weedVal == 0 && lsdVal == 0 && mushroomVal == 0 && heroineVal == 0 && cocaineVal == 1) // Cocaine
            {
                std::cout << "Cocaine:         ";
            }
            else if(weedVal == 1 && lsdVal == 1 && mushroomVal == 0 && heroineVal == 0 && cocaineVal == 0) // WeedLSD
            {
                std::cout << "WeedLSD:         ";
            }
            else if(weedVal == 0 && lsdVal == 0 && mushroomVal == 1 && heroineVal == 0 && cocaineVal == 1) // MushroomCocaine
            {
                std::cout << "MushroomCocaine: ";
            }
            else if(weedVal == 1 && lsdVal == 0 && mushroomVal == 0 && heroineVal == 0 && cocaineVal == 1) // WeedCocaine
            {
                std::cout << "WeedCocaine:     ";
            }
        }

        /**
         * @brief This method is for the container to be able to create a type Drug<weed, lsd, mushroom, heroine, cocaine>
         *        from the Item*. Wanted to use covariance to return pointer to object, but Item* cannot be used to 
         *        return a Drug*.
         * 
         * @param weed variable is set to the value of the weedVal in the Item object.
         * @param lsd variable is set to the value of the lsdVal in the Item object.
         * @param mushroom variable is set to the value of the mushroomVal in the Item object.
         * @param heroine variable is set to the value of the heroineVal in the Item object.
         * @param cocaine variable is set to the value of the cocaineVal in the Item object.
         */
        void checkValues(int& weed, int& lsd, int& mushroom, int& heroine, int& cocaine) override
        {
            weed = weedVal;
            lsd = lsdVal;
            mushroom = mushroomVal;
            heroine = heroineVal;
            cocaine = cocaineVal;
        }

        void setStartPrice() override
        {
            if(weedVal == 1 && lsdVal == 0 && mushroomVal == 0 && heroineVal == 0 && cocaineVal == 0) // Weed
            {
                setBasePrice(1000.0_kr);
                setSellPrice(1000.0_kr);
            }
            else if(weedVal == 0 && lsdVal == 1 && mushroomVal == 0 && heroineVal == 0 && cocaineVal == 0) // LSD
            {
                setBasePrice(2000.0_kr);
                setSellPrice(2000.0_kr);
            }
            else if(weedVal == 0 && lsdVal == 0 && mushroomVal == 1 && heroineVal == 0 && cocaineVal == 0) // Mushroom
            {
                setBasePrice(3000.0_kr);
                setSellPrice(3000.0_kr);
            }
            else if(weedVal == 0 && lsdVal == 0 && mushroomVal == 0 && heroineVal == 1 && cocaineVal == 0) // Heroine
            {
                setBasePrice(4000.0_kr);
                setSellPrice(4000.0_kr);
            }
            else if(weedVal == 0 && lsdVal == 0 && mushroomVal == 0 && heroineVal == 0 && cocaineVal == 1) // Cocaine
            {
                setBasePrice(5000.0_kr);
                setSellPrice(5000.0_kr);
            }
            else if(weedVal == 1 && lsdVal == 1 && mushroomVal == 0 && heroineVal == 0 && cocaineVal == 0) // WeedLSD
            {
                setBasePrice(10000.0_kr);
                setSellPrice(10000.0_kr);
            }
            else if(weedVal == 0 && lsdVal == 0 && mushroomVal == 1 && heroineVal == 0 && cocaineVal == 1) // MushroomCocaine
            {
                setBasePrice(10000.0_kr);
                setSellPrice(10000.0_kr);
            }
            else if(weedVal == 1 && lsdVal == 0 && mushroomVal == 0 && heroineVal == 0 && cocaineVal == 1) // WeedCocaine
            {
                setBasePrice(10000.0_kr);
                setSellPrice(10000.0_kr);
            }
        }

        /**
         * @brief Updates Sell Price based on the quantity and the base price of the Drug.
         * 
         */
        void updatePrice() override
        {
            int amount = getQuantity();
            double basePrice = getBasePrice();
            double priceDrop = basePrice * (amount/100.0_g * 0.005); // For every 10 grams of the drug, the price drops five percent.
            //std::cout << "priceDrop = " << priceDrop << std::endl;
            setSellPrice(basePrice-priceDrop);
        }

        // /**
        //  * @brief Returns Drug pointer for this object.
        //  * 
        //  * @return const Drug* is the pointer for this object.
        //  */
        // Drug* clone() override { return reinterpret_cast<Drug*>(new Drug());}

// Following works, but if accessed via an Item pointer, the Drug references would be a problem.
        // Adding two of same type. Quantities summed in this object.
        // Drug<Weed, LSD, Mushroom, Heroine, Cocaine> & operator += (const Drug<Weed, LSD, Mushroom, Heroine, Cocaine> & refDrug)
        // {
        //     int tempQuantity = refDrug.getQuantity();
        //     tempQuantity += getQuantity();
        //     setQuantity(tempQuantity);
        //     return *this;
        // }

        // Subtracting two of same type. Quantities subtracted from this object.
        // Drug<Weed, LSD, Mushroom, Heroine, Cocaine> & operator -= (const Drug<Weed, LSD, Mushroom, Heroine, Cocaine> & refDrug)
        // {
        //     int tempQuantity = getQuantity();
        //     tempQuantity -= refDrug.getQuantity();
        //     setQuantity(tempQuantity);
        //     return *this;
        // }
    // By overriding the virtual += operator of Item and be sure to check that the two Items added are Drugs, the problem is solved.

//*******************************************************************************
// E X C E P T I O N
//*******************************************************************************

        /**
         * @brief Adding two of the same type. Quantities summed in this object.
         *        WARNING: Throws exeption if type does not have a quantity!
         *        
         * @param refItem is the right hand side object of the type Drug.
         * @return Drug& is the left hand side object of the type Drug.
         */
        Drug & operator += (const Item & refItem) override
        {
            if((refItem.getQuantity() >= 0))   
            {
                int tempQuantity = refItem.getQuantity();
                tempQuantity += getQuantity();
                setQuantity(tempQuantity);
                return *this;
            }
            else
            {
                throw std::invalid_argument( "Types added should both be Drugs." );     // Always throw exception by value. Catch by reference.
            }
        }

        /**
         * @brief Subtracting two of same type. Quantities subtracted from this object.
         *        WARNING: Throws excepltion if type does not have a quantity!
         * 
         * @param refItem is the right hand side object of the type Drug.
         * @return Drug& is the left hand side object of the type Drug.
         */
        Drug & operator -= (const Item & refItem) override
        {
            if((refItem.getQuantity() >= 0))
            {
                int tempQuantity = getQuantity();
                tempQuantity -= refItem.getQuantity();
                setQuantity(tempQuantity);
                return *this;
            }
            else
            {
                throw std::invalid_argument( "Types subtracted should both be Drugs" );     // Always throw exception by value. Catch by reference.
            }
        }

        static const int weedVal = Weed;
        static const int lsdVal = LSD;
        static const int mushroomVal = Mushroom;
        static const int heroineVal = Heroine;
        static const int cocaineVal = Cocaine;
    };  // End class Drug

//*******************************************************************************
// T Y P E    A L I A S
//*******************************************************************************

    // Below are the type aliases for Drug.
    // There are five basic types of drugs, each defined by the template parameters.
    typedef Drug<1,0,0,0,0> Weed;
    typedef Drug<0,1,0,0,0> LSD;
    typedef Drug<0,0,1,0,0> Mushroom;
    typedef Drug<0,0,0,1,0> Heroine;
    typedef Drug<0,0,0,0,1> Cocaine;

//*******************************************************************************
// T E M P L A T E   C L A S S   +   F I X E D   T R A I T S
//*******************************************************************************

    /**
     * @brief Generate a new Drug type from two Drugs.
     * 
     * @tparam T is the drug to be combined with U.
     * @tparam U is the drug to be combined with T.
     */
    template< typename T, typename U> 
    struct generateType
    {
        typedef Drug<   T::weedVal      + U::weedVal, 
                        T::lsdVal       + U::lsdVal, 
                        T::mushroomVal  + U::mushroomVal, 
                        T::heroineVal   + U::heroineVal, 
                        T::cocaineVal   + U::cocaineVal> type;
    };

//*******************************************************************************
// H A S   V A R I A B L E   - - -   S F I N A E
// V O I D _ T   ( C + + 1 7)
//*******************************************************************************

    /**
     * @brief Default for has_quantity. Inherits ::value from false_type.
     * 
     * @tparam T is the type checked for the member variable "quantity".
     * @tparam void 
     */
    template< typename T, typename = void >
    struct has_quantity : std::false_type { }; // If variable is not in class, inherit from false_type.

    /**
     * @brief Checking if the type T has the member variable "quantity".
     * 
     * @tparam T is the type checked for the member variable "quantity".
     */
    template<typename T>
    struct has_quantity<T, std::void_t<decltype(std::declval< T >().quantity)> > : std::true_type { }; 
    // If variable in class, inherit from true_type.


//*******************************************************************************
// T Y P E T R A I T   - - -   V A L U E
//*******************************************************************************

    /**
     * @brief Checking if the two types has the attribute "quantity". If it has, set value to true.
     * 
     * @tparam T is checked for 'quantity'.
     * @tparam U is checked for 'quantity'.
     */
    template<typename T, typename U>
    struct checkCompatibility
    {
        // method 1:
        // static_assert(has_quantity<T>::value, "First parameter does not have a member variable called 'quantity'!");
        // static_assert(has_quantity<U>::value, "Second parameter does not have a member variable called 'quantity'!");

        // method 2:
        static const bool value = (has_quantity<T>::value && has_quantity<U>::value);
    };

//*******************************************************************************
// I F T H E N E L S E   C U S T O M I Z E D   - - -   S F I N A E
//*******************************************************************************

    // /**
    //  * @brief If true, sets type as TrueResult. If false, sets type as FalseResult.
    //  * 
    //  * @tparam condition is the bool that is checked.
    //  * @tparam TrueResult is the type that is set, if the condition is true.
    //  * @tparam FalseResult is the type that is set, if the condition is false.
    //  */
    // template<bool condition, typename TrueResult, typename FalseResult>
    // struct ifThenElse;

    // template<typename TrueResult, typename FalseResult>
    // struct ifThenElse<true, TrueResult, FalseResult>
    // {
    //     typedef TrueResult type;
    // };

    // template<typename TrueResult, typename FalseResult>
    // struct ifThenElse<false, TrueResult, FalseResult>
    // {
    //     typedef FalseResult type;
    // };

    /**
     * @brief Checks if T and U are compatible types for creating new type. 
     *        If not compatible, type is set to void*. 
     *        If compatible, type is set to the new type.
     * 
     * @tparam condition is to be set by CheckCompatibility<T, U>.
     * @tparam T is the first of two types to be combined.
     * @tparam U is the second of two types to be combined.
     */
    template<bool condition, typename T, typename U>
    struct generateTypeGuarded
    {
        typedef void* type;
    };

    template<typename T, typename U>
    struct generateTypeGuarded<true, T, U>
    {
        typedef typename generateType<T, U>::type type;
    };

    /**
     * @brief Checks if the two types have a member variable "quantity" and then generates type of them.
     * 
     * @tparam T is the type to be mixed with U.
     * @tparam U is the type to be mixed with T.
     */
    template<typename T, typename U>
    struct allowThisMix
    {
        using type = typename generateTypeGuarded< checkCompatibility<T,U>::value , T , U >::type;
    };

        // using type = typename ifThenElse<checkCompatibility<T, U>::value, typename generateType<T, U>::type, void*>::type;
        // The line above doesn't work because generateType is always compiled wether the checkCompatibility is true or false.
        // If they are not compatible, there will be a compile error in the generateType struct.

//*******************************************************************************
// E N A B L E _ I F 
//*******************************************************************************
    
    /**
     * @brief Takes two Drugs. Checks if the combined Drug type is in the TypeList
     *        for allowed combined Drugs. Modifies the combined Drug's quantity based 
     *        on the two parameter Drugs. Then sets those Drugs' quantity to zero.
     *        WARNING: Throws exception if two Drug types are NOT mergeable!
     * 
     * @tparam Typelist is the TypeList containing all the allowed Drug mixes.
     * @tparam Drug1 is the type of drug to be combined with Drug2.
     * @tparam Drug2 is the type of drug to be combined with Drug1.
     * @param drug1 is the object of type Drug1.
     * @param drug2 is the object of type Drug2.
     * @return Returning a pointer to the newly created object of combined Drug type.
     */
    template<typename Typelist, typename Drug1, typename Drug2>
    typename std::enable_if<  
                    Contains<Typelist, typename generateType<Drug1, Drug2>::type >::value,          // First parameter for enable_if: Check if type generated is in Typelist.
                    typename generateType<Drug1, Drug2>::type*                                      // Second parameter for enable_if: The return type for alchemist.
                           >::type                                                                  // Return type for this alchemist is a pointer to the type generated
        alchemist(Drug1& drug1, Drug2& drug2)
    {
        std::cout << "alchemist has combined the two drugs successfully." << std::endl;
        using generatedType = typename generateType<Drug1, Drug2>::type;
        auto drugPtr =  new generatedType((drug1 += drug2).getQuantity());
        
        if constexpr(has_quantity<Drug1>::value)
            drug1.setQuantity(0.0_g);
        if constexpr(has_quantity<Drug2>::value)
            drug2.setQuantity(0.0_g);

        return drugPtr;
    }

    /**
     * @brief Takes two Drugs. Checks if the combined Drug type is in the TypeList
     *        for allowed combined Drugs. Modifies the combined Drug's quantity based 
     *        on the two parameter Drugs. Then sets those Drugs' quantity to zero.
     *        WARNING: Throws exception if two Drug types are NOT mergeable!
     * 
     * @tparam Typelist is the TypeList containing all the allowed Drug mixes.
     * @tparam Drug1 is the type of drug to be combined with Drug2.
     * @tparam Drug2 is the type of drug to be combined with Drug1.
     * @param drug1 is the object of type Drug1.
     * @param drug2 is the object of type Drug2.
     * @return Returning a pointer to the newly created object of combined Drug type.
     */
    template<typename Typelist, typename Drug1, typename Drug2>
    typename std::enable_if< 
                    !Contains<Typelist, typename generateType<Drug1, Drug2>::type >::value,         // First parameter for enable_if: Check if type generated is in Typelist.
                    void*                                                                           // Second parameter for enable_if: The return type for alchemist.
                           >::type                                                                  // Return type for this alchemist() is void*
        alchemist(Drug1& drug1, Drug2& drug2)
    {
        std::cout << "BOOOOOOOOOOOOOOOOOOOOOOOOOOOOM!!!! Your laboratory just exploded. The mix og the two drug types were not allowed." << std::endl;
        std::cout << "The two drugs are now gone from your inventory." << std::endl;
        drug1.setQuantity(0.0_g);
        drug2.setQuantity(0.0_g);

        throw std::invalid_argument( "The added types are not compatible! Your lab just exploded!!!" );     // Always throw exception by value. Catch by reference.
        // NOTE: Throwing an exception here is not the best practice. It's more costly than handling the return type. 
        // This is just to show how an exception is used.
    }

    /**
     * @brief For player to try to combine two types. If these have been 
     *        allowed by allowTheseMixes at compile time, the combined object is 
     *        returned. If the two types are not allowed, UNWANTED STUFF SHOULD HAPPEN!
     * 
     * @tparam T is a type that is to be combined with U, if compatible.
     * @tparam U is a type that is to be combined with T, if compatible.
     * @param t is an object of the type that is to be combined if the types T and U are compatible.
     * @param u is an object of the type that is to be combined if the types T and U are compatible.
     * @return auto is the returned object of the new, combined type.
     */
    // template<typename Typelist, typename Drug1, typename Drug2>
    // auto alchemist(Drug1& drug1, Drug2& drug2)
    // {
        // using generatedType = typename generateTypeGuarded< checkCompatibility<Drug1,Drug2>::value , Drug1 , Drug2 >::type;
        
        // if(Contains<Typelist, generatedType>::value)
        // {
        //     auto vPtr = new generatedType((drug1 += drug2).getQuantity());
        //     drug1.setQuantity(0.0_g);
        //     drug2.setQuantity(0.0_g);

        //     return vPtr;
        // }

        // else
        // {
        //     std::cout << "BOOOOOOOOOOOOOOOOOOOOOOOOOOOOM!!!! Your laboratory just exploded. The mix og the two drug types were not allowed." << std::endl;
        //     std::cout << "The two drugs are now gone from your inventory." << std::endl;
        //     drug1.setQuantity(0.0_g);
        //     drug2.setQuantity(0.0_g);
        //     throw std::invalid_argument( "The added types are not compatible! Your lab just exploded!!!" );     // Always throw exception by value. Catch by reference.

        //     //return nullptr; // inconsistent deduction for auto return type:
        // }


    //}

//*******************************************************************************
// A R G U M E N T   D E P E N D E N T   L O O K U P
//*******************************************************************************

    /**
     * @brief Prints out a greeting. Argument dependent lookup means compiler searches in argument's
     *        namespace for the function printWelcome first.
     * 
     * @param weedObject is not used but requred for function to be found without explicitly writing namespace.
     */
    static void printWelcome(Weed weedObject)
    {
        std::cout << "Welcome to DrugLord!" << std::endl;
        (void)weedObject;           // Just to avoid compiler warning. weedObject only used for argument dependent lookup.
    }

//*******************************************************************************
// C R E A T I N G   M I X E D   D R U G   T Y P E S
//*******************************************************************************
    typedef allowThisMix<Weed, LSD>::type WeedLSD;
    typedef allowThisMix<Mushroom, Cocaine>::type MushroomCocaine;
    typedef allowThisMix<Weed, Cocaine>::type WeedCocaine;

    typedef TYPELIST3(WeedLSD, MushroomCocaine, WeedCocaine) definedTypes;

} // End namespace DrugLord

#endif // __DRUG_HPP__