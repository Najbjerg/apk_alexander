#ifndef __DRUGTEST_HPP__
#define __DRUGTEST_HPP__

#include "../googletest/googletest/src/gtest-all.cc"
#include "Drug.hpp"

namespace dl = DrugLord;

// T E S T   E X A M P L E
// TEST(Title, Subtitle)
// {
//     ASSERT_EQ(2,2);
// }

TEST(Drugs, GenerateDrug)
{
    typedef dl::Drug<0,0,1,0,1> MushroomCocaine;
    using mushCocaine = dl::generateType<dl::Mushroom, dl::Cocaine>::type;
    mushCocaine thisObject(2.0_kg);
    ASSERT_TRUE((std::is_same_v<mushCocaine, MushroomCocaine>));
}

TEST(Drugs, Literals)
{
    dl::Heroine backpackHeroine(100.0_kg);
    ASSERT_EQ(backpackHeroine.getQuantity(), 100*1000.0_g);
}

TEST(Drugs, HasQuantity)
{
    static_assert(dl::has_quantity<dl::Weed>::value, "Weed does not have a quantity!");     // Testing for quantity in Weed. 
    ASSERT_TRUE(dl::has_quantity<dl::Weed>::value);

    ASSERT_FALSE(dl::has_quantity<int>::value);
}

TEST(Drugs, generateTypeGuarded)
{
    using weedLSD = dl::generateType<dl::Weed, dl::LSD>::type;
    using weedLSD2 = dl::generateTypeGuarded<dl::checkCompatibility<dl::Weed, dl::LSD>::value, dl::Weed, dl::LSD>::type;
    ASSERT_TRUE((std::is_same<weedLSD, weedLSD2>::value));

    using weedCocaine = dl::generateType<dl::Weed, dl::Cocaine>::type;
    using weedCocaine2 = dl::generateTypeGuarded<dl::checkCompatibility<dl::Weed, dl::Cocaine>::value, dl::Weed, dl::Cocaine>::type;
    ASSERT_TRUE((std::is_same<weedCocaine, weedCocaine2>::value));
}

TEST(Drugs, fail_generateTypeGuarded)
{
    static const bool trueOrFalse = dl::checkCompatibility<int, dl::LSD>::value;
    std::cout << "CheckCompatibitliy of int vs Weed. Result: " << trueOrFalse << std::endl;
    using intLSD = dl::generateTypeGuarded<trueOrFalse, int, dl::LSD>::type;
    ASSERT_TRUE((std::is_same<intLSD, void*>::value));
}

TEST(Drugs, checkCompatibility)
{
    dl::Heroine backpackHeroine(100.0_kg);
    dl::Weed backpackWeed(10.0_kg);
    bool checkThis = dl::checkCompatibility<decltype(backpackHeroine), decltype(backpackWeed)>::value;
    ASSERT_TRUE(checkThis);

    // NEXT LINE DOES NOT WORK BECAUSE OF R VALUE???
    // "undefined reference to 'DrugLord::checkCompatibility<DrugLord::Drug<0, 0, 0, 1, 0>, DrugLord::Drug<1, 0, 0, 0, 0> >::value'
    // ASSERT_TRUE((checkCompatibility<decltype(backpackHeroine), decltype(backpackWeed)>::value));
    
    int wrongType = 123;
    checkThis = dl::checkCompatibility<decltype(backpackHeroine), decltype(wrongType)>::value;
    ASSERT_FALSE(checkThis);

    ASSERT_FALSE((dl::checkCompatibility<int, dl::Weed>::value));
}

TEST(Items, plusEqualOperator)
{
    Item testItem1 = Item();
    Item testItem2 = Item();
    testItem1.setQuantity(10.0_kg);
    testItem2.setQuantity(1.0_kg);

    try{
        testItem1 += testItem2;
    } catch(std::exception& e)
    {
        std::cout << "Error trying testItem1 += testItem2: " << e.what() << std::endl;
    }
    ASSERT_EQ(testItem1.getQuantity(), 11.0_kg);
}

TEST(DrugPlusDrug, plusEqualOperator)
{
    dl::Heroine backpackHeroineOne(1.0_kg);
    dl::Heroine backpackHeroineTwo(100.0_kg);

    try{
        backpackHeroineOne += backpackHeroineTwo;
    } catch(std::exception& e)
    {
        std::cout << "Error trying backpackHeroineOne += backpackHeroineTwo: " << e.what() << std::endl;
    }
    
    ASSERT_EQ(backpackHeroineOne.getQuantity(), 101.0_kg);
}

TEST(DrugPlusItem, plusEqualOperator)
{
    dl::Heroine backpackHeroineOne(1.0_kg);
    Item testItem1 = Item();
    testItem1.setQuantity(10.0_kg);
    try{
        backpackHeroineOne += testItem1;
    } catch(std::exception& e)
    {
        std::cout << "Error trying backpackHeroineOne += testItem1: " << e.what() << std::endl;
    }
    
    ASSERT_EQ(backpackHeroineOne.getQuantity(), 11.0_kg);
}

TEST(PtrPlusPtr, plusEqualOperator)
{
    dl::Heroine backpackHeroineOne(1.0_kg);
    Item testItem1 = Item();
    testItem1.setQuantity(10.0_kg);

    Item* itemPtr1 = &backpackHeroineOne;
    Item* itemPtr2 = &testItem1;
    
    try{
        *itemPtr1 += *itemPtr2;
    } catch(std::exception& e)
    {
        std::cout << "Error trying backpackHeroineOne += testItem1: " << e.what() << std::endl;
    }

    ASSERT_EQ(((*itemPtr1).getQuantity()), 11.0_kg);
}

TEST(Item, printing)
{
    dl::Heroine backpackHeroineOne(1.0_kg);
    std::cout << "backpackHeroineOne has: " << backpackHeroineOne << std::endl;
    Item* testItemPtr = new Item();
    testItemPtr->setQuantity(10.0_kg);
    std::cout << "item class has: " << *testItemPtr << std::endl;
}

TEST(Drug, CopyConstructor)
{
    dl::Weed test1(1.0_kg);
    dl::Weed test2 = dl::Weed();
    std::cout << "Weed object test1 has: " << test1 << std::endl;
    std::cout << "Weed object test2 has: " << test2 << std::endl;
    std::cout << "Copying test1 into test2..." << std::endl;
    test2 = test1;
    std::cout << "test2 now has: " << test2 << std::endl;
}

TEST(TypeList, setup)
{
    bool testResult = (Contains<dl::definedTypes, dl::WeedLSD>::value);
    std::cout << "Result of Contains<dl::definedTypes, WeedLSD>::value == " << testResult << std::endl;
    ASSERT_TRUE(testResult);

    testResult = Contains<dl::definedTypes, dl::Weed>::value;
    std::cout << "Result of Contains<dl::definedTypes, Weed>::value == " << testResult << std::endl;
    ASSERT_FALSE(testResult);
}

TEST(alchemist, playing)
{
    using WeedLSD = dl::allowThisMix<dl::Weed, dl::LSD>::type;
    dl::Weed* weedTestPtr = new dl::Weed(1.0_kg);
    dl::LSD* lsdTestPtr = new dl::LSD(0.5_kg);
    std::cout << "Weed has: " << *weedTestPtr << std::endl;
    std::cout << "LSD has: " << *lsdTestPtr << std::endl;

    try
    {
        auto newItemPtr = dl::alchemist<dl::definedTypes, dl::Weed, dl::LSD>(*weedTestPtr, *lsdTestPtr);
        std::cout << newItemPtr << std::endl;

        std::cout << "Weed has: " << *weedTestPtr << std::endl;
        std::cout << "LSD has: " << *lsdTestPtr << std::endl;
        std::cout << "WeedLSD has: " << *newItemPtr << std::endl;

        ASSERT_TRUE((std::is_same<std::remove_reference<decltype(*newItemPtr)>::type, WeedLSD>::value));
    } catch(std::exception& e){
        std:: cout << "EXCEPTION!!!!! Error: " << e.what() << std::endl;
    }
}

TEST(alchemist, BOOM)
{
    using WeedWeed = dl::allowThisMix<dl::Weed, dl::Weed>::type;
    dl::Weed* weed1TestPtr = new dl::Weed(1.0_kg);
    dl::Weed* weed2TestPtr = new dl::Weed(2.0_kg);
    std::cout << "Weed1 has: " << *weed1TestPtr << std::endl;
    std::cout << "Weed2 has: " << *weed2TestPtr << std::endl;

    Item* newItemPtr;
    try
    {
        newItemPtr = (Item*)dl::alchemist<dl::definedTypes, dl::Weed, dl::Weed>(*weed1TestPtr, *weed2TestPtr);  // Simply for testing, I've casted the void* to Item*.
        
        std::cout << *newItemPtr << std::endl;

        ASSERT_TRUE((std::is_same<std::remove_reference<decltype(*newItemPtr)>::type, WeedWeed>::value));
     
    } catch(std::exception& e){
        std:: cout << "EXCEPTION!!!!! Error: " << e.what() << std::endl;
        std::cout << "Weed1 has: " << *weed1TestPtr << std::endl;
        std::cout << "Weed2 has: " << *weed2TestPtr << std::endl;
    }
}

TEST(ArgumentDependentLookup, test)
{
    dl::Weed weedTest = dl::Weed();
    printWelcome(weedTest);         // No namespace needed before printWelcome because argument is from DrugLord namespace.
}

TEST(test, test)
{
    Item* testPtr = new dl::Weed(2.0_kg);
    testPtr->printType();
    std::cout << *testPtr << std::endl;
    ASSERT_EQ(testPtr->getSellPrice(), 1000);
    testPtr->updatePrice();
    std::cout << *testPtr << std::endl;
}

TEST(PointerConverter, printing)
{
    Item* testPtr = new dl::Weed(2.0_kg);
    int weed, lsd, mushroom, heroine, cocaine;
    testPtr->checkValues(weed, lsd, mushroom, heroine, cocaine);

    dl::Weed* weedPtr = nullptr;
    dl::LSD* lsdPtr = nullptr;
    dl::Mushroom* mushroomPtr = nullptr;
    dl::Heroine* heroinePtr = nullptr;
    dl::Cocaine* cocainePtr = nullptr;
    dl::WeedLSD* weedLsdPtr = nullptr;
    dl::MushroomCocaine* mushroomCocainePtr = nullptr;
    dl::WeedCocaine* weedCocainePtr = nullptr;

    std::cout << "Drug<" << weed << ", " << lsd << ", "<< cocaine << ", "<< heroine << ", "<< cocaine << "> " << std::endl;

    if(weed == 1 && lsd == 0 && mushroom == 0 && heroine == 0 && cocaine == 0)          // If Weed
        weedPtr = new dl::Weed(testPtr->getQuantity());                                 // Create Weed pointer
    else if(weed == 0 && lsd == 1 && mushroom == 0 && heroine == 0 && cocaine == 0)     // If LSD
        lsdPtr = new dl::LSD(testPtr->getQuantity());                                   // Create LSD pointer
    else if(weed == 0 && lsd == 0 && mushroom == 1 && heroine == 0 && cocaine == 0)     // If Mushroom
        mushroomPtr = new dl::Mushroom(testPtr->getQuantity());                         // Create Mushroom pointer
    else if(weed == 0 && lsd == 0 && mushroom == 0 && heroine == 1 && cocaine == 0)     // If Heroine
        heroinePtr = new dl::Heroine(testPtr->getQuantity());                           // Create Heroine pointer
    else if(weed == 0 && lsd == 0 && mushroom == 0 && heroine == 0 && cocaine == 1)     // If Cocaine
        cocainePtr = new dl::Cocaine(testPtr->getQuantity());                           // Create Cocaine pointer
    else if(weed == 1 && lsd == 1 && mushroom == 0 && heroine == 0 && cocaine == 0)     // If WeedLSD
        weedLsdPtr = new dl::Drug<1,1,0,0,0>(testPtr->getQuantity());                   // Create WeedLSD pointer
    else if(weed == 0 && lsd == 0 && mushroom == 1 && heroine == 0 && cocaine == 1)     // If MushroomCocaine
        mushroomCocainePtr = new dl::Drug<0,0,1,0,1>(testPtr->getQuantity());           // Create MushroomCocaine pointer
    else if(weed == 1 && lsd == 0 && mushroom == 0 && heroine == 0 && cocaine == 1)     // If WeedCocaine
        weedCocainePtr = new dl::Drug<1,0,0,0,1>(testPtr->getQuantity());               // Create WeedCocaine pointer

    if(weedPtr != nullptr)
        weedPtr->printType();
    else if (lsdPtr != nullptr)
        lsdPtr ->printType();
    else if (mushroomPtr != nullptr)
        mushroomPtr ->printType();
    else if (heroinePtr != nullptr)
        heroinePtr ->printType();
    else if (cocainePtr != nullptr)
        cocainePtr ->printType();
    else if (weedLsdPtr != nullptr)
        weedLsdPtr ->printType();
    else if (mushroomCocainePtr != nullptr)
        mushroomCocainePtr ->printType();
    else if (weedCocainePtr != nullptr)
        weedCocainePtr ->printType();
}

// TEST(lvalue, move)
// {
//     int *myPtr = new int(15);

//     int *otherPtr(std::move(myPtr));
//     //otherPtr = std::move(myPtr);
//     ASSERT_EQ(*myPtr, 15);
// }

#endif